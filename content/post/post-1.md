---
title: "Post 1"
date: 2023-07-25T10:35:14+02:00
draft: false
---

Ceci est le premier post

# Ceci est un titre

## Et cela un sous-titre

Voilà du code en python: 

```python
from flask import Flask
import os
import redis

app = Flask(__name__)

# Connexion à la base de données Redis
redis_db = redis.Redis(host=os.environ.get('REDIS_HOST', 'localhost'), port=6379)

@app.route('/')
def home(): 
    # Incrémentation du compteur
    counter = redis_db.incr('page_counter')
    
    return f'La page a été rechargée {counter} fois.\n'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

```